import { FaceMesh } from "@mediapipe/face_mesh";
import React, { useRef, useEffect, useState } from 'react';
import * as Facemesh from "@mediapipe/face_mesh";
import * as cam from "@mediapipe/camera_utils";
import Webcam from "react-webcam";
import eyebrowSource from "../images/eyebrow_color.png";
import { useOpenCv } from 'opencv-react';
var ColorThief = require('color-thief');
var cv2;

// const brow_area = [8, 9, 151, 337, 299, 333, 298, 301, 389, 368, 383, 353, 445, 444, 443, 442, 441, 417];
const brow_area = [8, 285, 295, 282, 283, 276, 300, 293, 334, 296, 336, 9, 151, 337, 299, 333, 298, 301, 389, 368, 383, 353, 445, 444, 443, 442, 441, 417];
const only_left_brow = [285, 295, 282, 283, 276, 300, 293, 334, 296, 336];


const CameraScreen = () => {
  const {loaded, cv} = useOpenCv()
  console.log(loaded, cv);

  const webcamRef = useRef(null);
  const canvasRef = useRef(null);
  var camera = null;

  let skinColor = undefined;

  function onResults(results) {

    // Set canvas width
    canvasRef.current.width = window.innerHeight/480 * 640;
    canvasRef.current.height = window.innerHeight;

    const canvasElement = canvasRef.current;
    const canvasCtx = canvasElement.getContext("2d");
    canvasCtx.save();
    canvasCtx.clearRect(0, 0, canvasElement.width, canvasElement.height);
    let image = document.getElementById('source');
    
    canvasCtx.drawImage(
      results.image,
      0,
      0,
      canvasElement.width,
      canvasElement.height
    );
    if (results.multiFaceLandmarks) {
      for (const landmarks of results.multiFaceLandmarks) {

        let imgData = canvasCtx.getImageData(0, 0, canvasElement.width, canvasElement.height);
        let imageCV = cv2.matFromImageData(imgData);
        let dst = new cv2.Mat();
        imageCV.convertTo(dst, cv2.CV_8UC4);
        cv2.cvtColor(dst, dst, cv2.COLOR_RGB2RGB);

        // let imageData2 = canvasCtx.createImageData(mask.cols, mask.rows);
        // imageData2.data.set(new Uint8ClampedArray(mask.data, mask.cols, mask.rows));
        // canvasCtx.putImageData(imageData2, 0, 0);


        if(!skinColor) {
          //  NA RAZIE KOLOR DO ZAMAZYWANIA BRWI JEST POBIERANY TYLKO RAZ, PRZY PIERWSZYM ODPALENIU KAMERY 
          const color = getImageDominantColor(imageCV, results);
          skinColor = color;
          console.log(color);
        }

        if(skinColor) {
          let mask = new cv2.Mat(imageCV.rows, imageCV.cols, imageCV.type(), new cv2.Scalar(0, 0, 0, 255));
          const mapped_response = format_response(results.multiFaceLandmarks, canvasElement);
          const points = [];
          for (const dot of only_left_brow) {
            points.push(mapped_response[dot].x, mapped_response[dot].y)
          }

          let square_point_data = new Uint16Array(points);
          let square_points = cv2.matFromArray(square_point_data.length/2, 1, cv2.CV_32SC2, square_point_data);
          let pts = new cv2.MatVector();
          pts.push_back(square_points);
          let color = new cv2.Scalar(skinColor[0], skinColor[1], skinColor[2], 255);
          //   FILLPOLY WYPEŁNIA OBSZAR MIĘDZY PUNKTAMI WYBRANYM KOLOREM
          cv2.fillPoly(mask, pts, color);

          //--------ROZMAZYWANIE MASKI----------
          // let ksize = new cv2.Size(9, 9);
          // let anchor = new cv2.Point(-1, -1);
          // cv2.blur(mask, mask, ksize, anchor, cv2.BORDER_DEFAULT);

          // const input_bgra = new cv2.Mat();
          // cv2.cvtColor(imageCV, input_bgra, cv2.CV_BGR2BGRA); //  KONWERSJA imageCV ŻEBY MIEĆ 4 KANAŁY I NIŻEJ ZOSTAJE NAM SAMA SKÓRA NA KANALE ALFA


          // find all black pixel and set alpha value to zero:
          // for (var y = 0; y < mask.rows; ++y) {
          //   for (var x = 0; x < mask.cols; ++x)
          //   {
          //       let pixel = mask.ucharPtr(y, x);
          //       if (pixel[0] == 0 && pixel[1] == 0 && pixel[2] == 0)
          //       {
          //           // set alpha to zero:
          //           pixel[3] = 0;
          //       }
          //   }
          // }


          // let imageData2 = canvasCtx.createImageData(mask.cols, mask.rows);
          // imageData2.data.set(new Uint8ClampedArray(mask.data, mask.cols, mask.rows));
          // canvasCtx.putImageData(imageData2, 0, 0);


          // let imageData2 = canvasCtx.createImageData(mask.cols, mask.rows);
          // imageData2.data.set(new Uint8ClampedArray(mask.data, mask.cols, mask.rows));
          // canvasCtx.putImageData(imageData2, 0, 0);
          dst = new cv2.Mat();
          let out = imageCV.clone()
          const alpha = 0.5
          cv2.addWeighted(imageCV, alpha, mask, 1 - alpha, 0, out) // POMYSŁ Z NAKŁADANIE OBRAZÓW JEDEN NA DRUGI - NA RAZIE DZIAŁA MARNIE

          let imageData2 = canvasCtx.createImageData(out.cols, out.rows);
          imageData2.data.set(new Uint8ClampedArray(out.data, out.cols, out.rows));
          canvasCtx.putImageData(imageData2, 0, 0); // TE TRZY LINIJKI SŁUŻĄ DO WYŚWIETLANIA MAT W CANVASIE 

          // out.delete();
          mask.delete();
          square_points.delete();


          //---------KAWAŁEK KODU Z BITMAPĄ--------
          // let bitmap;
          // createImageBitmap(imageData2).then(r => 
          //   {
              // canvasCtx.drawImage(
              //   r,
              //   0,
              //   0,
              //   canvasElement.width,
              //   canvasElement.height
          //     );
          //   });
        }

        imageCV.delete();
        dst.delete();


        
        let leftBrowPoints = Facemesh.FACEMESH_LEFT_EYEBROW.flat(1)
          .filter((item, pos) => Facemesh.FACEMESH_LEFT_EYEBROW.flat(1).indexOf(item) === pos);
        leftBrowPoints = leftBrowPoints.map(i => landmarks[i])

        let rightBrowPoints = Facemesh.FACEMESH_RIGHT_EYEBROW.flat(1)
          .filter((item, pos) => Facemesh.FACEMESH_RIGHT_EYEBROW.flat(1).indexOf(item) === pos);
        rightBrowPoints = rightBrowPoints.map(i => landmarks[i])
        

        //-------KAWAŁEK KODU ODPOWIEDZIALNY ZA NAKŁADANIE GOTOWYCH BRWI------
        //lewa brew
        // let minX = 1, maxX = 0, maxY = 0, minY = 1;
        // for(const point of leftBrowPoints) {
        //   if(point.x < minX) minX = point.x;
        //   if(point.x > maxX) maxX = point.x;
        //   if(point.y > maxY) maxY = point.y;
        //   if(point.y < minY) minY = point.y;
        // }

        // minX = minX * canvasElement.width
        // maxX = maxX * canvasElement.width
        // minY = minY * canvasElement.height
        // maxY = maxY * canvasElement.height
        // canvasCtx.drawImage(
        //   image,
        //   minX,
        //   minY,
        //   maxX - minX,
        //   maxY - minY
        // );

        // //prawa, obrócona brew
        // minX = 1; maxX = 0; maxY = 0; minY = 1;
        // for(const point of rightBrowPoints) {
        //   if(point.x < minX) minX = point.x;
        //   if(point.x > maxX) maxX = point.x;
        //   if(point.y > maxY) maxY = point.y;
        //   if(point.y < minY) minY = point.y;
        // }

        // minX = minX * canvasElement.width
        // maxX = maxX * canvasElement.width
        // minY = minY * canvasElement.height
        // maxY = maxY * canvasElement.height
        // canvasCtx.scale(-1, 1);
        // canvasCtx.drawImage(
        //   image,
        //   -minX ,
        //   minY,
        //   -(maxX - minX),
        //   maxY - minY
        // );
        
      }
      canvasCtx.restore();
    }
  }

  const getImageDominantColor = (image, results) => {
    const canvasElement = canvasRef.current;
    const canvasCtx = canvasElement.getContext("2d");
    let imageCV = image;

    //  CZARNA MASKA
    let mask = new cv2.Mat(imageCV.rows, imageCV.cols, imageCV.type(), new cv2.Scalar(0, 0, 0, 255));
    const mapped_response = format_response(results.multiFaceLandmarks, canvasElement);
    const points = [];
    for (const dot of brow_area) {
      points.push(mapped_response[dot].x, mapped_response[dot].y)
    }

    let square_point_data = new Uint16Array(points);
    let square_points = cv2.matFromArray(square_point_data.length/2, 1, cv2.CV_32SC2, square_point_data);

    let pts = new cv2.MatVector();
    pts.push_back(square_points);
    let color = new cv2.Scalar(255, 255, 255, 255);
    //   FILLPOLY WYPEŁNIA OBSZAR MIĘDZY PUNKTAMI WYBRANYM KOLOREM - TUTAJ WYPEŁNIA BIAŁYM KOLOREM OBSZAR, GDZIE ZNAJDUJE SIĘ SKÓRA DOOKOŁA BRWI
    cv2.fillPoly(mask, pts, color);

    let none = new cv2.Mat();
    cv2.bitwise_and(imageCV, mask, imageCV, none); // ŁĄCZYMY MASKĘ Z ORYGINALNYM OBRAZEM - ZOSTAJE NAM TYLKO SKÓRA NA CZARNYM TLE

    const input_bgra = new cv2.Mat();
    cv2.cvtColor(imageCV, input_bgra, cv2.CV_BGR2BGRA); //  KONWERSJA imageCV ŻEBY MIEĆ 4 KANAŁY I NIŻEJ ZOSTAJE NAM SAMA SKÓRA NA KANALE ALFA


    // find all black pixel and set alpha value to zero:
    for (var y = 0; y < input_bgra.rows; ++y) {
      for (var x = 0; x < input_bgra.cols; ++x)
      {
          let pixel = input_bgra.ucharPtr(y, x);
          if (pixel[0] == 0 && pixel[1] == 0 && pixel[2] == 0)
          {
              // set alpha to zero:
              pixel[3] = 0;
          }
      }
    }

    //  input_bgra TO TERAZ SKÓRA NA KANALE ALFA

    let imageData2 = canvasCtx.createImageData(input_bgra.cols, input_bgra.rows);
    imageData2.data.set(new Uint8ClampedArray(input_bgra.data, input_bgra.cols, input_bgra.rows));

    canvasCtx.putImageData(imageData2, 0, 0); //  WRZUCAMY WYCIĘTĄ SKÓRĘ DO CANVAS, BO BIBLIOTEKA DO WYZNACZANIA KOLORU DZIAŁA NA CANVAS

    var colorThief = new ColorThief();
    const responseColor = colorThief.getColor(canvasRef.current); //  DOMINANT COLOR POTRZEBNY DO ZAMAZANIA BRWI 

    square_points.delete();
    pts.delete();
    mask.delete();
    input_bgra.delete();
    none.delete();

    return responseColor;
  }

  const format_response = (response, canvasElement) => {
    const mapped_response = []
    for(const face of response) {
      for(const landmark of face) {
        const x = landmark.x
        const y = landmark.y

        mapped_response.push({
          x: x * canvasElement.width,
          y: y * canvasElement.height
        })
      }
    return mapped_response
    }
  }

  useEffect(() => {
    if(cv) {
      cv.then(c => cv2 = c);
      const faceMesh = new FaceMesh({
        locateFile: (file) => {
          return `https://cdn.jsdelivr.net/npm/@mediapipe/face_mesh/${file}`;
        },
      });

      faceMesh.setOptions({
        maxNumFaces: 1,
        minDetectionConfidence: 0.5,
        minTrackingConfidence: 0.5,
      });

      faceMesh.onResults(onResults);

      if (
        typeof webcamRef.current !== "undefined" &&
        webcamRef.current !== null
      ) {
        camera = new cam.Camera(webcamRef.current.video, {
          onFrame: async () => {
            await faceMesh.send({ image: webcamRef.current.video });
          },
        });
        camera.start();
      }
    }
  }, [cv]);

  return (
    <center>
      <div className="App">
        <Webcam
          ref={webcamRef}
          style={{
            position: "absolute",
            marginLeft: "auto",
            marginRight: "auto",
            left: 0,
            right: 0,
            textAlign: "center",
            zindex: 9,
            height: '100%',
            display: 'none'
          }}
        />{" "}
        <img id="source" src={eyebrowSource}/>
        <canvas
          ref={canvasRef}
          id="output_canvas"
          className="output_canvas"
          height='100%'
          style={{
            position: "absolute",
            marginLeft: "auto",
            marginRight: "auto",
            left: 0,
            right: 0,
            textAlign: "center",
            zindex: 9,
            height: '100%',
          }}
        ></canvas>
      </div>
    </center>
  );
}

export default CameraScreen;
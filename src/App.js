import './App.css';
import CameraScreen from './screens/CameraScreen';
import { OpenCvProvider, useOpenCv } from 'opencv-react'

function App() {
  const onLoaded = (cv) => {
    console.log('opencv loaded, cv')
  }

  return (
    <OpenCvProvider onLoad={onLoaded} openCvPath='/opencv/opencv.js'>
      <CameraScreen/>
    </OpenCvProvider>
  );
}

export default App;
